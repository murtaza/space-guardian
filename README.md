# Space Guardian - Unity

Designed and developed a game inspired by the 1978 classic, Space Invaders using Unity.

### Main menu
![](images/SGMainMenu.gif)

### Gameplay
![](images/SpaceGuardGameplay.gif)


### Death Screen
![](images/SGDeathMenu.gif)

## How to start the game?
1. Download the CompiledGame directory
2. Extract contents of Space_Guardian.zip into a folder 
3. Open up the Space Guardian.exe

## How to play?
Move up, down, left, and right with W (up),  S (down), A (left) and D (right) keys.

Use SpaceBar to shoot the enemies

The game also holds your highscore for each difficulty!

HINT: you can shoot the projectiles that enemies shoot at you as well.
